![t.gif](https://bitbucket.org/repo/8zX7LyK/images/1669814750-t.gif)

# Overview #

**Flinty FX** is an editor for 3D particle effects. It was created in my spare time as a personal project while studying Computer Science at the University of Montana.

# How do I use it? #

When you launch Flinty FX, you are presented with a window containing a preview area and a sidebar listing available parameters. The preview area updates in real-time as parameters are adjusted. Navigate around the visualization by dragging or scrolling inside the preview pane. Visit the [downloads](https://bitbucket.org/GregMcMann/flinty-fx/downloads/) page to get the latest version of the application and sample files.

# How does it work? #

The user interface uses JavaFX, the successor to Swing. In order to reduce lag in the visualization, all of the particles present on the screen are batched into a single draw call by constructing them all in the same mesh. To further improve performance, particles are only created or destroyed when the size of the particle pool changes by adjusting the 'count' parameter.