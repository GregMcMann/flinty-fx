import document.Document;
import document.Emitter;
import document.Shape;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import view.Inspector;
import view.Menu;
import view.Preview;
import view.Timer;

import java.io.File;
import java.io.IOException;

public class Main extends Application {

    private final String appName = "Flinty FX";

    private Document document;
    private Inspector inspector;
    private Preview preview;
    private Timer timer;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {

        Menu menu = new Menu();
        menu.onNew(e -> newDocument(stage));
        menu.onOpen(e -> openDocument(stage));
        menu.onSave(e -> saveDocument(stage));
        menu.onSaveAs(e -> saveDocumentAs(stage));
        menu.onExport(e -> exportDocument(stage));
        menu.onBurst(e -> burstParticles());
        menu.onBrightness((v,a,b) -> adjustBrightness(b.doubleValue()));

        preview = new Preview();

        inspector = new Inspector(stage);

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(menu);
        borderPane.setRight(inspector);
        borderPane.setCenter(preview);

        KeyCombination newCombo    = new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN);
        KeyCombination openCombo   = new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN);
        KeyCombination saveCombo   = new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN);
        KeyCombination saveAsCombo = new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN);
        KeyCombination exportCombo = new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN);
        KeyCombination burstCombo  = new KeyCodeCombination(KeyCode.B, KeyCombination.SHORTCUT_DOWN);

        Scene scene = new Scene(borderPane, 1000, 700);
        scene.setOnKeyPressed(
                keyEvent -> {
                    if (newCombo.match(keyEvent)) {
                        newDocument(stage);
                    }
                    if (openCombo.match(keyEvent)) {
                        openDocument(stage);
                    }
                    if (saveCombo.match(keyEvent)) {
                        saveDocument(stage);
                    }
                    if (saveAsCombo.match(keyEvent)) {
                        saveDocumentAs(stage);
                    }
                    if (exportCombo.match(keyEvent)) {
                        exportDocument(stage);
                    }
                    if (burstCombo.match(keyEvent)) {
                        burstParticles();
                    }
                }
        );

        timer = new Timer();
        timer.setPreview(preview);
        timer.start();

        newDocument(stage);

        adjustBrightness(0.1);

        stage.setTitle(appName);
        stage.setScene(scene);
        stage.show();

    }

    public void newDocument(Stage stage) {

        document = new Document();

        Emitter emitter = document.emitter;

        emitter.particleCount      = 10;
        emitter.particleLife       = 1;
        emitter.particleRadius     = 0.01;
        emitter.particleSpeed      = 10.0;
        emitter.particleImage      = null;
        emitter.emitterBurst       = 0;
        emitter.emitterOffset      = 0;
        emitter.emitterElapsed     = 0;
        emitter.emitterContinuous  = true;
        emitter.emitterFrequency   = 0.1;
        emitter.emitterSize        = 1.0;
        emitter.emitterShape       = Shape.POINT;
        emitter.physicsMass        = 1.0;
        emitter.physicsDrag        = 0.0;
        emitter.physicsRestitution = 0.0;
        emitter.physicsForceX      = 0.0;
        emitter.physicsForceY      = 0.0;
        emitter.physicsForceZ      = 0.0;
        emitter.physicsFloor       = -1.0;
        emitter.physicsCeiling     = 1.0;
        emitter.blendStart         = Color.WHITE;
        emitter.blendEnd           = Color.WHITE;
        emitter.fadeStart          = 0.0;
        emitter.fadeEnd            = 0.0;
        emitter.scaleStart         = 0.0;
        emitter.scaleEnd           = 0.0;
        emitter.radiusAmplitude    = 0.0;
        emitter.radiusFrequency    = 0.0;
        emitter.opacityAmplitude   = 0.0;
        emitter.opacityFrequency   = 0.0;

        timer.setDocument(document);

        preview.setNode(document.emitter);

        inspector.bind(document.emitter);
        inspector.sync();

        stage.setTitle(appName);

    }

    public void openDocument(Stage stage) {

        FileChooser chooser = new FileChooser();
        chooser.setTitle("Open");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Flinty FX","*.ini"));
        File file = chooser.showOpenDialog(stage);

        if (file != null) {

            try {

                document = new Document(file.getAbsolutePath());

                timer.setDocument(document);

                preview.setNode(document.emitter);

                inspector.bind(document.emitter);
                inspector.sync();

                stage.setTitle(appName + " - " + file.getName());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveDocument(Stage stage) {
        if (document.sourceFile == null) {
            saveDocumentAs(stage);
        } else {
            try {
                document.save(document.sourceFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveDocumentAs(Stage stage) {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save As..");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Flinty FX","*.ini"));
        File file = fileChooser.showSaveDialog(stage);

        if (file != null) {
            try {
                document.save(file.getAbsolutePath());
                stage.setTitle(appName + " - " + file.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void exportDocument(Stage stage) {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Emitter","*.emit"));
        File file = fileChooser.showSaveDialog(stage);

        if (file != null) {
            try {
                document.export(file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void burstParticles() {
        document.emitter.burst();
    }

    public void adjustBrightness(double brightness) {

        double tint = 0.05;

        brightness = Math.min(brightness + tint, 1.0 - tint);

        int fgColor = (int)((brightness + tint) * 255);
        int bgColor = (int)(brightness * 255);

        preview.setForeground(Color.rgb(fgColor,fgColor,fgColor));
        preview.setBackground(Color.rgb(bgColor,bgColor,bgColor));

    }
}
