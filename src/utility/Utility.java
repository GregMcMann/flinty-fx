package utility;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class Utility {

    public static void putString(ByteBuffer buffer, String string) {

        try {

            if (string != null) {

                for (byte b : string.getBytes("UTF-8")) {

                    buffer.put(b);
                }

            }

        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();

        }

        buffer.put((byte) 0);

    }

}
