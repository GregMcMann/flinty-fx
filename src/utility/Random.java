package utility;

public class Random {

    public static double range(double min, double max) {

        return Math.random() * (max - min) + min;

    }
}
