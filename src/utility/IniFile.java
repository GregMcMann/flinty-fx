package utility;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IniFile {

    private Pattern sectionRegex = Pattern.compile("\\s*\\[(.*)\\]");
    private Pattern keyRegex = Pattern.compile("\\s*(.*)=(.*)");
    private Map<String,Map<String,String>> entries = new LinkedHashMap<>();

    public IniFile() {

    }

    public IniFile(String path) throws IOException {

        load(path);

    }

    public void load(String path) throws IOException {

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {

            String line;
            String section = null;

            while ((line = br.readLine()) != null) {

                if (line.startsWith(";")) continue;

                Matcher m = sectionRegex.matcher(line);

                if (m.matches()) {

                    section = m.group(1).trim();

                } else if (section != null) {

                    m = keyRegex.matcher(line);

                    if (m.matches()) {

                        String key = m.group(1);
                        String value = m.group(2);

                        putString(section,key,value);

                    }
                }
            }
        }
    }

    public void save(String path) throws IOException {

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {

            for (String section : sections()) {

                bw.write("\n[" + section + "]\n\n");

                for (Map.Entry<String,String> entry : entries.get(section).entrySet()) {

                    bw.write(entry.getKey() + "=" + entry.getValue() + "\n");

                }
            }
        }
    }

    public Set<String> sections() {

        return entries.keySet();

    }

    public Set<String> keys(String section) {

        Map<String, String> map = entries.get(section);

        if (map == null) {

            return null;

        } else {

            return map.keySet();

        }
    }

    public void putString(String section, String key, String value) {

        Map<String, String> map = entries.get(section);

        if (map == null) {

            map = new LinkedHashMap<>();

            entries.put(section,map);

        }

        map.put(key,value);

    }

    public void putInt(String section, String key, int value) {

        putString(section,key,Integer.toString(value));

    }

    public void putFloat(String section, String key, float value) {

        putString(section,key,Float.toString(value));

    }

    public void putDouble(String section, String key, double value) {

        putString(section,key,Double.toString(value));

    }

    public void putBoolean(String section, String key, boolean value) {

        putString(section,key,Boolean.toString(value));

    }

    public String getString(String section, String key, String defaultValue) {

        Map<String, String> map = entries.get(section);

        if (map == null) return defaultValue;

        return map.getOrDefault(key,defaultValue);

    }

    public int getInt(String section, String key, int defaultValue) {

        return Integer.parseInt(getString(section,key,Integer.toString(defaultValue)));

    }

    public float getFloat(String section, String key, float defaultValue) {

        return Float.parseFloat(getString(section,key,Float.toString(defaultValue)));

    }

    public double getDouble(String section, String key, double defaultValue) {

        return Double.parseDouble(getString(section,key,Double.toString(defaultValue)));

    }

    public boolean getBoolean(String section, String key, boolean defaultValue) {

        return Boolean.parseBoolean(getString(section,key,Boolean.toString(defaultValue)));

    }

}