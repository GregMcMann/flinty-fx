package utility;

public class Vector3D {

    public double x;
    public double y;
    public double z;

    public Vector3D(double x, double y, double z) {

        this.x = x;
        this.y = y;
        this.z = z;

    }

    public Vector3D add(Vector3D other) {

        return new Vector3D(this.x + other.x,
                this.y + other.y,
                this.z + other.z);

    }

    public Vector3D sub(Vector3D other) {

        return new Vector3D(this.x - other.x,
                this.y - other.y,
                this.z - other.z);

    }

    public Vector3D cross(Vector3D other) {

        return new Vector3D(this.y * other.z - this.z * other.y,
                this.z * other.x - this.x * other.z,
                this.x * other.y - this.y * other.x);

    }

    public double dot(Vector3D other) {

        return this.x * other.x + this.y * other.y + this.z * other.z;

    }

    public Vector3D inverse() {

        return this.multiply(-1);

    }

    public Vector3D multiply(double scalar) {

        return new Vector3D(x * scalar,
                y * scalar,
                z * scalar);

    }

    public double norm() {

        return Math.sqrt(x * x + y * y + z * z);

    }

    public Vector3D normalized() {

        double norm = this.norm();

        if (norm != 0.0) {

            return this.multiply(1.0 / this.norm());

        } else {

            return new Vector3D(0.0, 0.0, 0.0);

        }
    }

    public Vector3D copy() {

        return new Vector3D(x, y, z);

    }

    public void set(double x, double y, double z) {

        this.x = x;
        this.y = y;
        this.z = z;

    }

    @Override
    public String toString() {
        return "Vec3D{x=" + x + ", y=" + y + ", z=" + z + "}";
    }
}
