package utility;

import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

public class Targa { // TODO: Extend WritableImage

    public static Image read(File file) throws IOException {

        FileInputStream stream = new FileInputStream(file);
        FileChannel channel = stream.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate((int)channel.size());

        buffer.order(ByteOrder.LITTLE_ENDIAN);
        channel.read(buffer);
        buffer.rewind();
        stream.close();

        byte  idlength        = buffer.get();
        byte  colourmaptype   = buffer.get();
        byte  datatypecode    = buffer.get();
        short colourmaporigin = buffer.getShort();
        short colourmaplength = buffer.getShort();
        byte  colourmapdepth  = buffer.get();
        short xorigin         = buffer.getShort();
        short yorigin         = buffer.getShort();
        short width           = buffer.getShort();
        short height          = buffer.getShort();
        byte  bitsperpixel    = buffer.get();
        byte  imagedescriptor = buffer.get();

        //System.out.println("Width " + width + " Height " + height + " Bits " + bitsperpixel);

        WritableImage image = new WritableImage(width,height);
        PixelWriter writer = image.getPixelWriter();

        int x;
        int y;

        for (int i = 0; i < width * height; i++) {

            x = i % width;
            y = height - i / width - 1;

            if (bitsperpixel == 8) {

                int r = buffer.get() & 0xFF;

                writer.setColor(x,y,Color.rgb(r,r,r));

            } else if (bitsperpixel == 24) {

                int b = buffer.get() & 0xFF;
                int g = buffer.get() & 0xFF;
                int r = buffer.get() & 0xFF;

                writer.setColor(x,y,Color.rgb(r,g,b));

            } else if (bitsperpixel == 32) {

                int b = buffer.get() & 0xFF;
                int g = buffer.get() & 0xFF;
                int r = buffer.get() & 0xFF;
                int a = buffer.get() & 0xFF;

                writer.setColor(x,y,Color.rgb(r,g,b,a / 255.0));

            }
        }

        return image;

    }

}
