package view;

import javafx.scene.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

public class Preview extends StackPane {

    private Group     root;
    private Node      target;
    private Camera    camera;
    private Rotate    rotateX;
    private Rotate    rotateY;
    private Translate translate;
    private SubScene  subScene;

    private double dragStartX;
    private double dragStartY;
    private double startRotateX;
    private double startRotateY;

    private Grid floor;
    private Grid ceiling;

    public Preview() {

        rotateX   = new Rotate(-20.0, Rotate.X_AXIS);
        rotateY   = new Rotate(-30.0, Rotate.Y_AXIS);
        translate = new Translate(0.0, 0.0, -8.0);

        camera = new PerspectiveCamera(true);
        camera.setNearClip(0.1);
        camera.setFarClip(1000.0);
        camera.getTransforms().addAll(

                rotateY,
                rotateX,
                translate

        );

        floor = new Grid(10, 10, 0.008);
        ceiling = new Grid(1, 1, 0.008);

        root = new Group(camera);
        root.getChildren().addAll(

                floor,
                ceiling,
                new AmbientLight(Color.WHITE)

        );

        subScene = new SubScene(root, 200, 200, true, SceneAntialiasing.DISABLED);
        subScene.setCamera(camera);
        subScene.setManaged(false);
        subScene.setOnScroll(this::scroll);
        subScene.setOnMousePressed(this::mousePressed);
        subScene.setOnMouseDragged(this::mouseDragged);
        subScene.widthProperty().bind(widthProperty());
        subScene.heightProperty().bind(heightProperty());

        getChildren().add(subScene);

    }

    private void scroll(ScrollEvent event) {

        double factor = 0.01;

        translate.setZ(translate.getZ() + event.getDeltaY() * factor);

    }

    private void mousePressed(MouseEvent event) {

        dragStartX = event.getSceneX();
        dragStartY = event.getSceneY();
        startRotateX = rotateX.getAngle();
        startRotateY = rotateY.getAngle();

    }

    private void mouseDragged(MouseEvent event) {

        double factor = 0.8;

        rotateX.setAngle(startRotateX + (event.getSceneY() - dragStartY) * -factor);
        rotateY.setAngle(startRotateY + (event.getSceneX() - dragStartX) *  factor);

    }

    public Affine getLookAtMatrix() {

        return new Affine(camera.getLocalToSceneTransform());

    }

    public void setBackground(Paint paint) {

        subScene.setFill(paint);

    }

    public void setForeground(Color color) {

        floor.setColor(color);
        ceiling.setColor(color);

    }

    public void setFloor(double height) {

        floor.setTranslateY(height);

    }

    public void setCeiling(double height) {

        ceiling.setTranslateY(height);

    }

    public void setNode(Node node) {

        if (target == null) {

            target = node;

            root.getChildren().add(target);

        } else {

            root.getChildren().remove(target);

            target = node;

            root.getChildren().add(target);

        }
    }


}
