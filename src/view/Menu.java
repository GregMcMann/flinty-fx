package view;

import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

public class Menu extends ToolBar {

    private Button btnNew;
    private Button btnOpen;
    private Button btnSave;
    private Button btnSaveAs;
    private Button btnExport;
    private Button btnBurst;
    private Slider sldBrightness;

    public Menu() {

        btnNew = new Button("New");
        btnNew.setGraphic(new ImageView("icons/page.png"));

        btnOpen = new Button("Open");
        btnOpen.setGraphic(new ImageView("icons/folder.png"));

        btnSave = new Button("Save");
        btnSave.setGraphic(new ImageView("icons/diskette.png"));

        btnSaveAs = new Button("SaveAs");
        btnSaveAs.setGraphic(new ImageView("icons/disk_multiple.png"));

        btnExport = new Button("Export");
        btnExport.setGraphic(new ImageView("icons/document_export.png"));

        btnBurst = new Button("Burst");
        btnBurst.setGraphic(new ImageView("icons/magic_wand_2.png"));

        sldBrightness = new Slider(0.0,1.0,0.1);

        Pane spacer = new Pane();
        HBox.setHgrow(spacer, Priority.SOMETIMES);

        this.getItems().addAll(

                btnNew,
                btnOpen,
                btnSave,
                btnSaveAs,
                new Separator(),
                btnExport,
                new Separator(),
                btnBurst,
                spacer,
                sldBrightness

        );

    }

    public void onNew(EventHandler<ActionEvent> eventHandler) {

        btnNew.setOnAction(eventHandler);

    }

    public void onOpen(EventHandler<ActionEvent> eventHandler) {

        btnOpen.setOnAction(eventHandler);

    }

    public void onSave(EventHandler<ActionEvent> eventHandler) {

        btnSave.setOnAction(eventHandler);

    }

    public void onSaveAs(EventHandler<ActionEvent> eventHandler) {

        btnSaveAs.setOnAction(eventHandler);

    }

    public void onExport(EventHandler<ActionEvent> eventHandler) {

        btnExport.setOnAction(eventHandler);

    }

    public void onBurst(EventHandler<ActionEvent> eventHandler) {

        btnBurst.setOnAction(eventHandler);

    }

    public void onBrightness(ChangeListener<? super Number> listener) {

        sldBrightness.valueProperty().addListener(listener);

    }

}
