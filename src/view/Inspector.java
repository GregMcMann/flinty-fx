package view;

import document.Emitter;
import input.*;
import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Inspector extends ScrollPane {

    private IntegerInput uiParticleCount      = new IntegerInput("count",0,1000);
    private DoubleInput  uiParticleLife       = new DoubleInput("life",0,1000);
    private DoubleInput  uiParticleRadius     = new DoubleInput("radius",0,1000);
    private DoubleInput  uiParticleSpeed      = new DoubleInput("speed",0,1000);
    private ImageInput   uiParticleImage;
    private BooleanInput uiEmitterContinuous  = new BooleanInput("continuous");
    private DoubleInput  uiEmitterFrequency   = new DoubleInput("frequency",0,1000);
    private DoubleInput  uiEmitterSize        = new DoubleInput("size",0,1000);
    private ShapeInput   uiEmitterShape       = new ShapeInput("shape");
    private DoubleInput  uiPhysicsMass        = new DoubleInput("mass",0,1000);
    private DoubleInput  uiPhysicsDrag        = new DoubleInput("drag",-1000,1000);
    private DoubleInput  uiPhysicsRestitution = new DoubleInput("restitution",-1000,1000);
    private DoubleInput  uiPhysicsForceX      = new DoubleInput("forceX",-1000,1000);
    private DoubleInput  uiPhysicsForceY      = new DoubleInput("forceY",-1000,1000);
    private DoubleInput  uiPhysicsForceZ      = new DoubleInput("forceZ",-1000,1000);
    private DoubleInput  uiPhysicsFloor       = new DoubleInput("floor",-1000,1000);
    private DoubleInput  uiPhysicsCeiling     = new DoubleInput("ceiling",-1000,1000);
    private ColorInput   uiBlendStart         = new ColorInput("blendStart");
    private ColorInput   uiBlendEnd           = new ColorInput("blendEnd");
    private DoubleInput  uiFadeStart          = new DoubleInput("fadeStart",-1000,1000);
    private DoubleInput  uiFadeEnd            = new DoubleInput("fadeEnd",-1000,1000);
    private DoubleInput  uiScaleStart         = new DoubleInput("scaleStart",-1000,1000);
    private DoubleInput  uiScaleEnd           = new DoubleInput("scaleEnd",-1000,1000);
    private DoubleInput  uiRadiusAmplitude    = new DoubleInput("radiusAmplitude",-1000,1000);
    private DoubleInput  uiRadiusFrequency    = new DoubleInput("radiusFrequency",-1000,1000);
    private DoubleInput  uiOpacityAmplitude   = new DoubleInput("opacityAmplitude",-1000,1000);
    private DoubleInput  uiOpacityFrequency   = new DoubleInput("opacityFrequency",-1000,1000);

    public Inspector(Stage stage) {

        super();

        uiParticleImage = new ImageInput("image",stage);

        VBox vbox = new VBox(5);
        vbox.setPadding(new Insets(5, 20, 5, 5));
        vbox.getChildren().addAll(

                new Divider("Emitter"),
                uiEmitterShape,
                uiEmitterSize,
                uiParticleCount,
                uiEmitterFrequency,
                uiEmitterContinuous,

                new Divider("Particle"),
                uiParticleImage,
                uiParticleRadius,
                uiParticleLife,
                uiParticleSpeed,

                new Divider("Physics"),
                uiPhysicsMass,
                uiPhysicsDrag,
                uiPhysicsRestitution,
                uiPhysicsForceX,
                uiPhysicsForceY,
                uiPhysicsForceZ,
                uiPhysicsFloor,
                uiPhysicsCeiling,

                new Divider("Blending"),
                uiBlendStart,
                uiBlendEnd,

                new Divider("Transitions"),
                uiFadeStart,
                uiFadeEnd,
                uiScaleStart,
                uiScaleEnd,

                new Divider("Oscillation"),
                uiRadiusAmplitude,
                uiRadiusFrequency,
                uiOpacityAmplitude,
                uiOpacityFrequency

        );

        setContent(vbox);
        setHbarPolicy(ScrollBarPolicy.NEVER);

    }

    public void bind(Emitter emitter) {

        try {

            Class c = Emitter.class;

            uiParticleCount     .bind(emitter,c.getField("particleCount"));
            uiParticleLife      .bind(emitter,c.getField("particleLife"));
            uiParticleRadius    .bind(emitter,c.getField("particleRadius"));
            uiParticleSpeed     .bind(emitter,c.getField("particleSpeed"));
            uiParticleImage     .bind(emitter,c.getField("particleImage"));
            uiEmitterContinuous .bind(emitter,c.getField("emitterContinuous"));
            uiEmitterFrequency  .bind(emitter,c.getField("emitterFrequency"));
            uiEmitterSize       .bind(emitter,c.getField("emitterSize"));
            uiEmitterShape      .bind(emitter,c.getField("emitterShape"));
            uiPhysicsMass       .bind(emitter,c.getField("physicsMass"));
            uiPhysicsDrag       .bind(emitter,c.getField("physicsDrag"));
            uiPhysicsRestitution.bind(emitter,c.getField("physicsRestitution"));
            uiPhysicsForceX     .bind(emitter,c.getField("physicsForceX"));
            uiPhysicsForceY     .bind(emitter,c.getField("physicsForceY"));
            uiPhysicsForceZ     .bind(emitter,c.getField("physicsForceZ"));
            uiPhysicsFloor      .bind(emitter,c.getField("physicsFloor"));
            uiPhysicsCeiling    .bind(emitter,c.getField("physicsCeiling"));
            uiBlendStart        .bind(emitter,c.getField("blendStart"));
            uiBlendEnd          .bind(emitter,c.getField("blendEnd"));
            uiFadeStart         .bind(emitter,c.getField("fadeStart"));
            uiFadeEnd           .bind(emitter,c.getField("fadeEnd"));
            uiScaleStart        .bind(emitter,c.getField("scaleStart"));
            uiScaleEnd          .bind(emitter,c.getField("scaleEnd"));
            uiRadiusAmplitude   .bind(emitter,c.getField("radiusAmplitude"));
            uiRadiusFrequency   .bind(emitter,c.getField("radiusFrequency"));
            uiOpacityAmplitude  .bind(emitter,c.getField("opacityAmplitude"));
            uiOpacityFrequency  .bind(emitter,c.getField("opacityFrequency"));

        } catch (NoSuchFieldException e) {

            e.printStackTrace();

        }

    }

    public void sync() {

        uiParticleCount     .sync();
        uiParticleLife      .sync();
        uiParticleRadius    .sync();
        uiParticleSpeed     .sync();
        uiParticleImage     .sync();
        uiEmitterContinuous .sync();
        uiEmitterFrequency  .sync();
        uiEmitterSize       .sync();
        uiEmitterShape      .sync();
        uiPhysicsMass       .sync();
        uiPhysicsDrag       .sync();
        uiPhysicsRestitution.sync();
        uiPhysicsForceX     .sync();
        uiPhysicsForceY     .sync();
        uiPhysicsForceZ     .sync();
        uiPhysicsFloor      .sync();
        uiPhysicsCeiling    .sync();
        uiBlendStart        .sync();
        uiBlendEnd          .sync();
        uiFadeStart         .sync();
        uiFadeEnd           .sync();
        uiScaleStart        .sync();
        uiScaleEnd          .sync();
        uiRadiusAmplitude   .sync();
        uiRadiusFrequency   .sync();
        uiOpacityAmplitude  .sync();
        uiOpacityFrequency  .sync();

    }

}
