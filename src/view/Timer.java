package view;

import document.Document;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {

    private Document document;
    private Preview preview;

    @Override
    public void handle(long now) {

        if (document != null && preview != null) {
            
            document.emitter.step(1.0 / 60.0);
            document.emitter.build(preview.getLookAtMatrix());

            preview.setFloor(-document.emitter.physicsFloor);
            preview.setCeiling(-document.emitter.physicsCeiling);

        }
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public void setPreview(Preview preview) {
        this.preview = preview;
    }
    
}
