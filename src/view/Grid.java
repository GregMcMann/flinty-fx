package view;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;

public class Grid extends Group {

    private PhongMaterial material;

    public Grid(int width, int depth, double thickness) {

        material = new PhongMaterial();

        for (int i = 0; i < width + 1; i++) {

            Box box = new Box(thickness, thickness, width);
            box.setMaterial(material);
            box.setTranslateX(-width + width / 2.0 + i);

            getChildren().add(box);

        }

        for (int i = 0; i < depth + 1; i++) {

            Box box = new Box(depth, thickness, thickness);
            box.setMaterial(material);
            box.setTranslateZ(-depth + depth / 2.0 + i);

            getChildren().add(box);

        }
    }

    public void setColor(Color color) {

        material.setDiffuseColor(color);

    }
}
