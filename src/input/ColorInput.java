package input;

import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

import java.lang.reflect.Field;

public class ColorInput extends HBox {

    private ColorPicker colorPicker;
    private Object instance;
    private Field field;

    public ColorInput(String name) {

        super();

        Label label = new Label(name);
        label.setPrefWidth(120);

        colorPicker = new ColorPicker();
        colorPicker.setPrefWidth(100);
        colorPicker.valueProperty().addListener(

                (v,oldV,newV) -> {

                    try {

                        field.set(instance,newV);

                    } catch (IllegalAccessException e) {

                        e.printStackTrace();

                    }

                }

        );

        getChildren().addAll(label,colorPicker);

    }

    public void bind(Object instance, Field field) {

        this.instance = instance;
        this.field = field;

    }

    public void sync() {

        try {

            colorPicker.setValue((Color)field.get(instance));

        } catch (IllegalAccessException e) {

            e.printStackTrace();

        }

    }
}
