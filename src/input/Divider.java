package input;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class Divider extends HBox {

    public Divider(String name) {

        super();

        setPrefHeight(25);
        setAlignment(Pos.CENTER);
        setStyle("-fx-background-color: #CCCCCC");

        getChildren().add(new Label(name));

    }
}
