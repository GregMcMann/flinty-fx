package input;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.lang.reflect.Field;

public class ImageInput extends HBox {

    private Object instance;
    private Field field;

    public ImageInput(String name, Stage stage) {

        super();

        Label label = new Label(name);
        label.setPrefWidth(120);

        Button button = new Button("Select");
        button.setPrefWidth(100);
        button.setOnAction(

                e -> {

                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Select a File");
                    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image", "*.bmp", "*.gif", "*.jpg", "*.tga", "*.png"));
                    File file = fileChooser.showOpenDialog(stage);

                    if (file != null) {

                        try {

                            field.set(instance, file);

                        } catch (IllegalAccessException ex) {

                            ex.printStackTrace();

                        }

                    }

                }

        );

        getChildren().addAll(label,button);

    }

    public void bind(Object instance, Field field) {

        this.instance = instance;
        this.field = field;

    }

    public void sync() {}

}
