package input;

import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.HBox;

import java.lang.reflect.Field;

public class IntegerInput extends HBox {

    Spinner<Integer> spinner;
    private Object instance;
    private Field field;

    public IntegerInput(String name, int min, int max) {

        super();

        Label label = new Label(name);
        label.setPrefWidth(120);

        spinner = new Spinner<>(min,max,0);
        spinner.setEditable(true);
        spinner.setPrefWidth(100);
        spinner.valueProperty().addListener(

                (v,oldValue,newValue) -> {

                    try {

                        field.setInt(instance, newValue);

                    } catch (IllegalAccessException e) {

                        e.printStackTrace();

                    }

                }

        );

        getChildren().addAll(label,spinner);

    }

    public void bind(Object instance, Field field) {

        this.instance = instance;
        this.field = field;

    }

    public void sync() {

        try {

            spinner.getValueFactory().setValue((Integer)field.get(instance));

        } catch (IllegalAccessException e) {

            e.printStackTrace();

        }

    }

}
