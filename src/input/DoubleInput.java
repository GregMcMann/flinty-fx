package input;

import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.ParseException;

public class DoubleInput extends HBox {

    private Spinner<Double> spinner;
    private Object instance;
    private Field field;

    public DoubleInput(String name, double min, double max) {

        super();

        Label label = new Label(name);
        label.setPrefWidth(120);

        spinner = new Spinner<>(min,max,0.0,0.1);
        spinner.setEditable(true);
        spinner.setPrefWidth(100);
        spinner.valueProperty().addListener(

                (v,oldV,newV) -> {

                    try {

                        field.setDouble(instance, newV);

                    } catch (IllegalAccessException e) {

                        e.printStackTrace();

                    }

                }

        );
        spinner.getValueFactory().setConverter(

                new StringConverter<Double>() {

                    private final DecimalFormat df = new DecimalFormat("#.####");

                    @Override
                    public String toString(Double value) {

                        if (value == null) {
                            return "";
                        }

                        return df.format(value);
                    }

                    @Override
                    public Double fromString(String value) {

                        try {

                            if (value == null) {
                                return null;
                            }

                            value = value.trim();

                            if (value.length() < 1) {
                                return null;
                            }

                            return df.parse(value).doubleValue();

                        } catch (ParseException ex) {

                            throw new RuntimeException(ex);

                        }
                    }
                }
        );

        getChildren().addAll(label,spinner);

    }

    public void bind(Object instance, Field field) {

        this.instance = instance;
        this.field = field;

    }

    public void sync() {

        try {

            spinner.getValueFactory().setValue((Double)field.get(instance));

        } catch (IllegalAccessException e) {

            e.printStackTrace();

        }

    }

}
