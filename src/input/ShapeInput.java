package input;

import document.Shape;
import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.lang.reflect.Field;
import java.util.Arrays;

public class ShapeInput extends HBox {

    private ChoiceBox<String> choiceBox;
    private Object instance;
    private Field field;

    public ShapeInput(String name) {

        super();

        Label label = new Label(name);
        label.setPrefWidth(120);

        choiceBox = new ChoiceBox<>();
        choiceBox.setPrefWidth(100);
        choiceBox.setItems(FXCollections.observableArrayList(Arrays.asList("POINT", "RAY", "LINE", "PLANE", "BOX", "RING")));
        choiceBox.setValue("POINT");
        choiceBox.valueProperty().addListener(

                (v,oldValue,newValue) -> {

                    try {

                        switch (newValue) {

                            case "POINT": field.set(instance, Shape.POINT); break;
                            case "RAY":   field.set(instance, Shape.RAY);   break;
                            case "LINE":  field.set(instance, Shape.LINE);  break;
                            case "PLANE": field.set(instance, Shape.PLANE); break;
                            case "BOX":   field.set(instance, Shape.BOX);   break;
                            case "RING":  field.set(instance, Shape.RING);  break;

                        }

                    } catch (IllegalAccessException e) {

                        e.printStackTrace();

                    }

                }

        );

        getChildren().addAll(label,choiceBox);

    }

    public void bind(Object instance, Field field) {

        this.instance = instance;
        this.field = field;

    }

    public void sync() {

        try {

            Shape shape = (Shape)field.get(instance);

            choiceBox.valueProperty().setValue((String)shape.toString());

        } catch (IllegalAccessException e) {

            e.printStackTrace();

        }

    }

}
