package input;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.lang.reflect.Field;

public class BooleanInput extends HBox {

    private CheckBox checkBox;

    private Object instance;
    private Field field;

    public BooleanInput(String name) {

        super();

        Label label = new Label(name);
        label.setPrefWidth(120);

        checkBox = new CheckBox();
        checkBox.setPrefWidth(100);
        checkBox.selectedProperty().addListener(
                (v,oldV,newV) -> {
                    try {
                        field.setBoolean(instance, newV);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
        );

        getChildren().addAll(label,checkBox);

    }

    public void bind(Object instance, Field field) {

        this.instance = instance;
        this.field = field;

    }

    public void sync() {

        try {

            checkBox.selectedProperty().setValue((Boolean)field.get(instance));

        } catch (IllegalAccessException e) {

            e.printStackTrace();

        }

    }

}
