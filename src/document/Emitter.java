package document;

import javafx.geometry.Point3D;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.shape.VertexFormat;
import javafx.scene.transform.Affine;
import utility.Random;
import utility.Vector3D;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Emitter extends MeshView {

    public int      particleCount;
    public double   particleLife;
    public double   particleRadius;
    public double   particleSpeed;
    public File     particleImage;
    public int      emitterBurst;
    public int      emitterOffset;
    public double   emitterElapsed;
    public boolean  emitterContinuous;
    public double   emitterFrequency;
    public double   emitterSize;
    public Shape    emitterShape = Shape.POINT;
    public double   physicsMass;
    public double   physicsDrag;
    public double   physicsRestitution;
    public double   physicsForceX;
    public double   physicsForceY;
    public double   physicsForceZ;
    public double   physicsFloor;
    public double   physicsCeiling;
    public Color    blendStart = Color.WHITE;
    public Color    blendEnd = Color.WHITE;
    public double   fadeStart;
    public double   fadeEnd;
    public double   scaleStart;
    public double   scaleEnd;
    public double   radiusAmplitude;
    public double   radiusFrequency;
    public double   opacityAmplitude;
    public double   opacityFrequency;

    private List<Particle> particles = new ArrayList<>();
    private PhongMaterial phongMaterial;
    private TriangleMesh triangleMesh;

    private File prevImageFile;

    public Emitter(int maxParticles) {

        super();

        for (int i = 0; i < maxParticles; i++) {

            particles.add(new Particle());

        }

        phongMaterial = new PhongMaterial();
        triangleMesh = new TriangleMesh(VertexFormat.POINT_TEXCOORD);

        setMaterial(phongMaterial);
        setMesh(triangleMesh);

    }

    private void emitParticle(double stepInSeconds) {

        //----------------------------------------------------
        // Reset the current particle
        //----------------------------------------------------

        Particle particle = particles.get(emitterOffset);

        particle.position.x = 0.0;
        particle.position.y = 0.0;
        particle.position.z = 0.0;
        particle.velocity.x = 0.0;
        particle.velocity.y = 0.0;
        particle.velocity.z = 0.0;
        particle.color.x = 1.0;
        particle.color.y = 1.0;
        particle.color.z = 1.0;
        particle.opacity = 1.0;
        particle.radius = particleRadius;
        particle.life = particleLife;

        //----------------------------------------------------
        // Set the position and velocity using the shape
        //----------------------------------------------------

        switch (emitterShape) {

            case POINT:
                particle.position.x = 0.0;
                particle.position.y = 0.0;
                particle.position.z = 0.0;
                particle.velocity.x = Random.range(-1.0, 1.0);
                particle.velocity.y = Random.range(-1.0, 1.0);
                particle.velocity.z = Random.range(-1.0, 1.0);
                break;

            case RAY:
                particle.position.x = 0.0;
                particle.position.y = 0.0;
                particle.position.z = 0.0;
                particle.velocity.x = 0.0;
                particle.velocity.y = Random.range(0.0, 1.0);
                particle.velocity.z = 0.0;
                break;

            case LINE:
                particle.position.x = Random.range(-1.0, 1.0) * 0.5 * emitterSize;
                particle.position.y = 0.0;
                particle.position.z = 0.0;
                particle.velocity.x = 0.0;
                particle.velocity.y = Random.range(0.0, 1.0);
                particle.velocity.z = 0.0;
                break;

            case PLANE:
                particle.position.x = Random.range(-1.0, 1.0) * 0.5 * emitterSize;
                particle.position.y = 0.0;
                particle.position.z = Random.range(-1.0, 1.0) * 0.5 * emitterSize;
                particle.velocity.x = 0.0;
                particle.velocity.y = Random.range(0.0, 1.0);
                particle.velocity.z = 0.0;
                break;

            case BOX:
                particle.position.x = Random.range(-1.0, 1.0) * 0.5 * emitterSize;
                particle.position.y = Random.range(-1.0, 1.0) * 0.5 * emitterSize;
                particle.position.z = Random.range(-1.0, 1.0) * 0.5 * emitterSize;
                particle.velocity.x = Random.range(-1.0, 1.0);
                particle.velocity.y = Random.range(-1.0, 1.0);
                particle.velocity.z = Random.range(-1.0, 1.0);
                break;

            case RING:
                particle.position.x = Random.range(-1.0, 1.0);
                particle.position.y = 0.0;
                particle.position.z = Random.range(-1.0, 1.0);

                particle.position = particle.position.normalized().multiply(0.5 * emitterSize);

                particle.velocity.x = particle.position.x;
                particle.velocity.y = particle.position.y;
                particle.velocity.z = particle.position.z;
                break;

        }

        particle.velocity = particle.velocity.normalized().multiply(particleSpeed * stepInSeconds);

        //----------------------------------------------------
        // Transform with the origin
        //----------------------------------------------------

        // Not implemented

        //----------------------------------------------------
        // Increment the offset
        //----------------------------------------------------

        emitterOffset = (emitterOffset + 1) % particleCount;

    }

    private double weightValue(double remaining, double duration, double inSeconds, double outSeconds) {

        if (remaining > duration) {

            return 0.0;

        } else if (remaining > duration - inSeconds) {

            return (duration - remaining) / inSeconds;

        } else if (remaining > outSeconds) {

            return 1.0;

        } else if (remaining > 0.0f) {

            return remaining / outSeconds;

        } else {

            return 0.0;

        }
    }

    public void step(double stepInSeconds) {

        //----------------------------------------------------
        // Emit particles
        //----------------------------------------------------

        emitterElapsed = emitterElapsed + stepInSeconds;

        emitterFrequency = Math.max(0.001, emitterFrequency);

        particleCount = Math.max(1, particleCount);

        emitterOffset = emitterOffset % particleCount;

        while (emitterElapsed >= emitterFrequency) {

            emitterElapsed = emitterElapsed - emitterFrequency;

            if (emitterContinuous) {

                emitParticle(stepInSeconds);

            } else if (emitterBurst > 0) {

                emitParticle(stepInSeconds);

                emitterBurst = emitterBurst - 1;

            }
        }

        //----------------------------------------------------
        // Step each particle
        //----------------------------------------------------

        for (Particle particle : particles) {

            // Physics

            double speed = particle.velocity.norm();

            Vector3D dragForce = particle.velocity.normalized().multiply(-1 * physicsDrag * speed * speed);

            physicsMass = Math.max(0.001, physicsMass);

            particle.position.x += particle.velocity.x * stepInSeconds;
            particle.position.y += particle.velocity.y * stepInSeconds;
            particle.position.z += particle.velocity.z * stepInSeconds;
            particle.velocity.x += 1.0 / physicsMass * (physicsForceX + dragForce.x) * stepInSeconds;
            particle.velocity.y += 1.0 / physicsMass * (physicsForceY + dragForce.y) * stepInSeconds;
            particle.velocity.z += 1.0 / physicsMass * (physicsForceZ + dragForce.z) * stepInSeconds;

            // Linear Blending

            // -- not implemented

            // End Transitions

            particle.opacity = weightValue(particle.life, particleLife, fadeStart, fadeEnd);
            particle.radius = weightValue(particle.life, particleLife, scaleStart, scaleEnd) * particleRadius;

            // Oscillation

            particle.opacity += opacityAmplitude * Math.sin(particle.life * opacityFrequency);
            particle.radius += radiusAmplitude * Math.sin(particle.life * radiusFrequency);

            // Timing

            particle.life = Math.max(0.0, particle.life - stepInSeconds);

            if (particle.life <= 0.0) {

                particle.radius = 0.0;

            }

            // Collision

            if (particle.position.y < physicsFloor + particle.radius) {

                particle.position.y = physicsFloor + particle.radius;

                particle.velocity.y = -particle.velocity.y * physicsRestitution;

            }

            if (particle.position.y > physicsCeiling - particle.radius) {

                particle.position.y = physicsCeiling - particle.radius;

                particle.velocity.y = -particle.velocity.y * physicsRestitution;

            }
        }
    }

    public void burst() {

        emitterBurst = particleCount;

    }

    public void build(Affine lookAt) {

        Affine affine = new Affine();
        affine.setMxx(lookAt.getMxx());
        affine.setMxy(lookAt.getMxy());
        affine.setMxz(lookAt.getMxz());
        affine.setMyx(lookAt.getMyx());
        affine.setMyy(lookAt.getMyy());
        affine.setMyz(lookAt.getMyz());
        affine.setMzx(lookAt.getMzx());
        affine.setMzy(lookAt.getMzy());
        affine.setMzz(lookAt.getMzz());

        triangleMesh.getPoints().clear();
        triangleMesh.getTexCoords().clear();
        triangleMesh.getFaces().clear();

        phongMaterial.setDiffuseColor(blendStart);

        File imageFile = particleImage;

        if (imageFile != prevImageFile) {

            if (imageFile != null) {

                Image image = null;

                System.out.println("Load");

                try {

                    image = new Image(new FileInputStream(imageFile.getAbsolutePath()));

                    phongMaterial.setDiffuseMap(image);

                } catch (FileNotFoundException e) {

                    phongMaterial.setDiffuseMap(null);

                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText("Missing Image");
                    alert.setContentText(imageFile.getAbsolutePath());
                    alert.show();

                }
                /*
                try {

                    image = Targa.read(imageFile);

                    phongMaterial.setDiffuseMap(image);

                } catch (IOException e) {

                    phongMaterial.setDiffuseMap(null);

                    e.printStackTrace();

                }*/

            } else {

                phongMaterial.setDiffuseMap(null);

            }

            prevImageFile = imageFile;

        }


        for (int i = 0; i < particleCount; i++) {

            Particle particle = particles.get(i);

            affine.setTx(particle.position.x);
            affine.setTy(-particle.position.y);
            affine.setTz(particle.position.z);

            Point3D p0 = new Point3D(-particle.radius, particle.radius, 0.0);
            Point3D p1 = new Point3D(-particle.radius, -particle.radius, 0.0);
            Point3D p2 = new Point3D(particle.radius, particle.radius, 0.0);
            Point3D p3 = new Point3D(particle.radius, -particle.radius, 0.0);

            p0 = affine.transform(p0.getX(), p0.getY(), p0.getZ());
            p1 = affine.transform(p1.getX(), p1.getY(), p1.getZ());
            p2 = affine.transform(p2.getX(), p2.getY(), p2.getZ());
            p3 = affine.transform(p3.getX(), p3.getY(), p3.getZ());

            triangleMesh.getPoints().addAll(

                    (float) p0.getX(), (float) p0.getY(), (float) p0.getZ(),
                    (float) p1.getX(), (float) p1.getY(), (float) p1.getZ(),
                    (float) p2.getX(), (float) p2.getY(), (float) p2.getZ(),
                    (float) p3.getX(), (float) p3.getY(), (float) p3.getZ()

            );

            triangleMesh.getTexCoords().addAll(

                    1, 1,
                    1, 0,
                    0, 1,
                    0, 0

            );

            triangleMesh.getFaces().addAll(

                    i * 4 + 2, i * 4 + 2, i * 4 + 1, i * 4 + 1, i * 4 + 0, i * 4 + 0,
                    i * 4 + 2, i * 4 + 2, i * 4 + 3, i * 4 + 3, i * 4 + 1, i * 4 + 1

            );

        }

    }
}
