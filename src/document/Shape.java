package document;

public enum Shape {

    POINT,
    RAY,
    LINE,
    PLANE,
    BOX,
    RING

}
