package document;

import utility.Vector3D;

public class Particle {

    public Vector3D position = new Vector3D(0.0,0.0,0.0);
    public Vector3D velocity = new Vector3D(0.0,0.0,0.0);
    public Vector3D color    = new Vector3D(0.0,0.0,0.0);
    public double   opacity  = 0.0;
    public double   radius   = 0.0;
    public double   life     = 0.0;

}
