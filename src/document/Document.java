package document;

import javafx.scene.paint.Color;
import utility.IniFile;
import utility.Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class Document {

    public File sourceFile;
    public Emitter emitter = new Emitter(1000);

    public Document() {}

    public Document(String path) throws IOException {

        load(path);

    }

    public void load(String path) throws IOException {

        IniFile file = new IniFile(path);

        sourceFile = new File(path);

        try {

            for (Field field : Emitter.class.getFields()) {

                int modifiers = field.getModifiers();

                if (Modifier.isStatic(modifiers)) {

                    continue;

                }

                if (field.getType() == boolean.class) {

                    boolean value = file.getBoolean("Emitter",field.getName(),false);

                    field.setBoolean(emitter,value);

                } else if (field.getType() == double.class) {

                    double value = file.getDouble("Emitter",field.getName(),0.0);

                    field.setDouble(emitter,value);

                } else if (field.getType() == int.class) {

                    int value = file.getInt("Emitter", field.getName(), 0);

                    field.setInt(emitter, value);

                } else if (field.getType() == Color.class) {

                    String hex = file.getString("Emitter",field.getName(),"0xffffffff");

                    field.set(emitter,Color.web(hex));

                } else if (field.getType() == File.class) {

                    String value = file.getString("Emitter",field.getName(),null);

                    if (value != null) {

                        field.set(emitter,new File(value));

                    }

                } else if (field.getType() == Shape.class) {

                    String value = file.getString("Emitter",field.getName(),"");

                    switch (value) {

                        case "POINT": field.set(emitter,Shape.POINT); break;
                        case "RAY":   field.set(emitter,Shape.RAY);   break;
                        case "LINE":  field.set(emitter,Shape.LINE);  break;
                        case "PLANE": field.set(emitter,Shape.PLANE); break;
                        case "BOX":   field.set(emitter,Shape.BOX);   break;
                        case "RING":  field.set(emitter,Shape.RING);  break;

                        default: field.set(emitter,Shape.POINT);

                    }
                }
            }

        } catch (IllegalAccessException e) {

            e.printStackTrace();

        }

    }

    public void save(String path) throws IOException {

        IniFile file = new IniFile();

        try {

            for (Field field : Emitter.class.getFields()) {

                int modifiers = field.getModifiers();

                if (Modifier.isStatic(modifiers)) {

                    continue;

                }

                if (field.getType() == boolean.class) {

                    file.putBoolean("Emitter",field.getName(),field.getBoolean(emitter));

                } else if (field.getType() == double.class) {

                    file.putDouble("Emitter",field.getName(),field.getDouble(emitter));

                } else if (field.getType() == int.class) {

                    file.putInt("Emitter",field.getName(),field.getInt(emitter));

                } else if (field.getType() == Color.class) {

                    Color color = (Color)field.get(emitter);

                    file.putString("Emitter",field.getName(),color.toString());

                } else if (field.getType() == File.class) {

                    File value = (File)field.get(emitter);

                    if (value != null) {

                        file.putString("Emitter",field.getName(),value.getAbsolutePath());

                    }

                } else if (field.getType() == Shape.class) {

                    Shape value = (Shape)field.get(emitter);

                    file.putString("Emitter",field.getName(),value.toString());

                }

            }

        } catch (IllegalAccessException e) {

            e.printStackTrace();

        }

        file.save(path);

        sourceFile = new File(path);

    }

    public void export(String path) throws IOException {

        ByteBuffer buffer = ByteBuffer.allocate(1000);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        buffer.putInt(emitter.particleCount);
        buffer.putFloat((float)emitter.particleLife);
        buffer.putFloat((float)emitter.particleRadius);
        buffer.putFloat((float)emitter.particleSpeed);

        if (emitter.emitterContinuous) {

            buffer.putInt(1);

        } else {

            buffer.putInt(0);

        }

        buffer.putFloat((float)emitter.emitterFrequency);
        buffer.putFloat((float)emitter.emitterSize);

        switch (emitter.emitterShape) {

            case POINT: buffer.putInt(0); break;
            case RAY:   buffer.putInt(1); break;
            case LINE:  buffer.putInt(2); break;
            case PLANE: buffer.putInt(3); break;
            case BOX:   buffer.putInt(4); break;
            case RING:  buffer.putInt(5); break;

        }

        buffer.putFloat((float)emitter.physicsMass);
        buffer.putFloat((float)emitter.physicsDrag);
        buffer.putFloat((float)emitter.physicsRestitution);
        buffer.putFloat((float)emitter.physicsForceX);
        buffer.putFloat((float)emitter.physicsForceY);
        buffer.putFloat((float)emitter.physicsForceZ);
        buffer.putFloat((float)emitter.physicsFloor);
        buffer.putFloat((float)emitter.physicsCeiling);

        buffer.putFloat((float)emitter.blendStart.getRed());
        buffer.putFloat((float)emitter.blendStart.getGreen());
        buffer.putFloat((float)emitter.blendStart.getBlue());
        buffer.putFloat((float)emitter.blendEnd.getRed());
        buffer.putFloat((float)emitter.blendEnd.getGreen());
        buffer.putFloat((float)emitter.blendEnd.getBlue());

        buffer.putFloat((float)emitter.fadeStart);
        buffer.putFloat((float)emitter.fadeEnd);
        buffer.putFloat((float)emitter.scaleStart);
        buffer.putFloat((float)emitter.scaleEnd);

        buffer.putFloat((float)emitter.radiusAmplitude);
        buffer.putFloat((float)emitter.radiusFrequency);
        buffer.putFloat((float)emitter.opacityAmplitude);
        buffer.putFloat((float)emitter.opacityFrequency);

        File imageFile = emitter.particleImage;

        if (imageFile == null) {

            Utility.putString(buffer,"");

        } else {

            Utility.putString(buffer,imageFile.getName());

        }

        byte[] bytes = Arrays.copyOfRange(buffer.array(), 0, buffer.position());

        FileOutputStream stream = new FileOutputStream(path);
        stream.write(bytes);
        stream.close();

    }

}
